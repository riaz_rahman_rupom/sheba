//
//  ImageSlideShowTableViewCell.swift
//  Sheba
//
//  Created by Rupom on 3/1/18.
//  Copyright © 2018 codepointbd. All rights reserved.
//

import UIKit
import ImageSlideshow

class ImageSlideShowTableViewCell: UITableViewCell {

    @IBOutlet var imageSlideShow: ImageSlideshow!
    let localSource = [ImageSource(imageString: "tiger")!, ImageSource(imageString: "sloth")!, ImageSource(imageString: "lion")!, ImageSource(imageString: "sloth")!]

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imageSlideShow.backgroundColor = UIColor.white
        imageSlideShow.slideshowInterval = 5.0
        imageSlideShow.pageControlPosition = PageControlPosition.underScrollView
        imageSlideShow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        imageSlideShow.pageControl.pageIndicatorTintColor = UIColor.black
        imageSlideShow.contentScaleMode = UIViewContentMode.scaleAspectFill
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        imageSlideShow.activityIndicator = DefaultActivityIndicator()
        imageSlideShow.currentPageChanged = { page in
            print("current page:", page)
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        imageSlideShow.setImageInputs(localSource)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
