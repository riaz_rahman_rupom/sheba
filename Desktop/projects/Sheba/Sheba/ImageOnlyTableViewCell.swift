//
//  ImageOnlyTableViewCell.swift
//  Sheba
//
//  Created by Rupom on 3/1/18.
//  Copyright © 2018 codepointbd. All rights reserved.
//

import UIKit

class ImageOnlyTableViewCell: UITableViewCell {
    @IBOutlet var imageName: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageName.image = UIImage(named: "sloth")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
