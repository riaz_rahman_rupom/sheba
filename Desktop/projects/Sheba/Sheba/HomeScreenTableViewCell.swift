//
//  HomeScreenTableViewCell.swift
//  Sheba
//
//  Created by Rupom on 2/14/18.
//  Copyright © 2018 codepointbd. All rights reserved.
//

import UIKit

class HomeScreenTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    @IBOutlet var CategoriescollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.CategoriescollectionView.dataSource = self
        self.CategoriescollectionView.register(UINib(nibName: "CategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCollectionViewCell")
       // self.CategoriescollectionView.isScrollEnabled = false
        self.CategoriescollectionView.isScrollEnabled = false
        self.CategoriescollectionView.isPagingEnabled = false
        self.CategoriescollectionView.delaysContentTouches = true
        self.CategoriescollectionView.canCancelContentTouches = false
        self.layoutCells()
        CategoriescollectionView.reloadData()
    }
    
    func layoutCells() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        layout.minimumInteritemSpacing = 5.0
        layout.minimumLineSpacing = 5.0
        //layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 40)/3, height: ((UIScreen.main.bounds.size.width - 40)/3))
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 40)/3,height: 150.0)
        CategoriescollectionView!.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 6
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.categoryText.text = "one"
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: 150.0, height: 150.0)
//    }
//
}
