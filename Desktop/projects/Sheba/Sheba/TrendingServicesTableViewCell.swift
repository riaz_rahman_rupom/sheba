//
//  TrendingServicesTableViewCell.swift
//  Sheba
//
//  Created by Rupom on 2/28/18.
//  Copyright © 2018 codepointbd. All rights reserved.
//

import UIKit

class TrendingServicesTableViewCell: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    @IBOutlet var trendingServicesCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.trendingServicesCollectionView.dataSource = self
        self.trendingServicesCollectionView.register(UINib(nibName: "CategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoriesCollectionViewCell")
        self.trendingServicesCollectionView.isScrollEnabled = true
        self.trendingServicesCollectionView.isPagingEnabled = false
        self.layoutCells()
        trendingServicesCollectionView.reloadData()
    }
    
    func layoutCells() {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 10, bottom: 10, right: 10)
        layout.minimumInteritemSpacing = 5.0
        layout.minimumLineSpacing = 5.0
        //layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 40)/3, height: ((UIScreen.main.bounds.size.width - 40)/3))
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.size.width - 40)/3 ,height: 150.0)
        layout.scrollDirection = .horizontal
        
        trendingServicesCollectionView!.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.categoryText.text = "one"
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}
