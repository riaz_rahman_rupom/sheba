//
//  CategoriesCollectionViewCell.swift
//  Sheba
//
//  Created by Rupom on 2/14/18.
//  Copyright © 2018 codepointbd. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var categoryText: UILabel!
    

}
