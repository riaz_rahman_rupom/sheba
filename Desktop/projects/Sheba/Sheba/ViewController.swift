//
//  ViewController.swift
//  Sheba
//
//  Created by Rupom on 2/14/18.
//  Copyright © 2018 codepointbd. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet var homeScreenTableView: UITableView!
    let order = ["imageSlider","categories","trendingServices","image","weddingServices"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpTableView()
    }
    
    func setUpTableView(){
        self.homeScreenTableView.delegate = self
        self.homeScreenTableView.dataSource = self
        let nib = UINib(nibName: "HomeScreenTableViewCell", bundle: nil)
        homeScreenTableView.register(nib, forCellReuseIdentifier: "HomeScreenTableViewCell")
        homeScreenTableView.register(UINib(nibName: "TrendingServicesTableViewCell", bundle : nil), forCellReuseIdentifier: "TrendingServicesTableViewCell")
        
        homeScreenTableView.register(UINib(nibName: "ImageSlideShowTableViewCell", bundle : nil), forCellReuseIdentifier: "ImageSlideShowTableViewCell")
        
        homeScreenTableView.register(UINib(nibName: "ImageOnlyTableViewCell", bundle : nil), forCellReuseIdentifier: "ImageOnlyTableViewCell")
        
        homeScreenTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if order[indexPath.row] == "imageSlider"{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageSlideShowTableViewCell", for: indexPath) as! ImageSlideShowTableViewCell
            return cell
            
        }
       else if order[indexPath.row] == "categories"{
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeScreenTableViewCell", for:indexPath) as! HomeScreenTableViewCell
        return cell
        }
        else if order[indexPath.row] == "trendingServices" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingServicesTableViewCell", for:indexPath) as! TrendingServicesTableViewCell
            return cell
        }
        else if order[indexPath.row] == "image" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageOnlyTableViewCell", for:indexPath) as! ImageOnlyTableViewCell
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TrendingServicesTableViewCell", for:indexPath) as! TrendingServicesTableViewCell
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if order[indexPath.row] == "imageSlider"{
            return 220
        }
        else  if order[indexPath.row] == "categories"{
            return 320
        } else   if order[indexPath.row] == "trendingServices"{
            return 150
        } else  if order[indexPath.row] == "image"{
            return 200
        } else {
            return 150
        }
    }

}

